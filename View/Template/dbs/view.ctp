<div class="row">
 <div id="panelMe" class="panel panel-primary no-border-radius">
  <div class="panel-heading no-border-radius">ITEMS</div>
  <div class="panel-body">
  <form>
   <div class="col-md-12">
     <div class="row">
       <div class="col-sm-1">
         <buttton class="btn btn-primary btn-sm btn-block" ng-click="addItemModal();"><i class="fa fa-plus"></i></button>
       </div>
     </div>   
     <table class="table table-stripe table-hover">
      <thead>
       <th></th>
       <th>CODE</th>
       <th>NAME</th>
       <th>DESCRIPTION</th>
       <th>SECTION</th>
      </thead>
     <tbody>
      <tr ng-repeat="datas in data">
       <td>{{ datas.id }}</td>
       <td>{{ datas.code}}</td>
       <td>{{ datas.name}}</td>
       <td>{{ datas.description }}</td>
       <td>{{ datas.section}}</td>
      </tr>
     </tbody>
     <tfoot>
     </tfoot>
    </table> 
  </div>
  </form>

  </div>
 </div>
</div>    
<style>
#panelMe{
 height:576px;
 margin-bottom:1px;
}
</style>
<?php echo $this->element('items/addItemModal')?>