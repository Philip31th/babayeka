<!DOCTYPE html>
<html>
<head>
	<title>LOGIN</title>
	<link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/login.css?version=<?php echo time() ?>">
	<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/jquery/jquery.min.js"></script>
</head>
<body>
	<section id="login">
    <header class="pull-left">
      <h1 style="font-size:47px;padding-bottom:10px">INTEGRATION</h1>
    </header>
  	<div class="col-xs-2 col-md-offset-3">
      <?php echo $this->Form->create('User', array('url'=>array('controller'=>'main', 'action'=>'login'), 'class'=>'', 'id'=>'', 'inputDefaults'=>array('label'=>false, 'div'=>false, 'class'=>'form-control' ))) ?>
          <div class="form-group">
          	<?php echo $this->Form->input('username', array('required'=>true, 'placeholder'=>'USERNAME', 'autofocus'=>true, 'class'=>'form-control input-form input-sm')) ?>
          </div>
  	</div>
  	<div class="col-xs-2">
      <div class="form-group">
      	<?php echo $this->Form->input('password', array('required'=>true, 'placeholder'=>'PASSWORD', 'class'=>'form-control input-form input-sm')) ?>
      </div>
    </div>
    <div class="col-md-2">
    	<button class="btn btn-primary  btn-min" id="loginBtn">
        SIGN IN
      </button> 
      <?php echo $this->Form->end() ?>
    </div>
    <div class="clearfix"></div>
  </section>
    
  <!-- Login -->

    
  <div class="footer">
  	<div class="copyright">COPYRIGHT &copy 2016 | ALL RIGHTS RESERVED</div>
  	<div class="poweredby">POWERED BY: <a href="http://www.marsmandrysdale.com/medical.php">MDMPI</a></div>
  </div>
</body>
</html>