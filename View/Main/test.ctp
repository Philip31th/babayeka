<div class="header">

	<div class="col-md-3">
		<div class="header-sub-wrapper">
			<a href="#/dashboard" class="logo-link">
				<div id="logo"><?php echo $this->Global->Settings('ecms') ?></div>
				<div id="title"><?php echo $this->Global->Settings('system_title') ?></div>	
			</a>
		</div>
	</div>

	<div class="col-md-9">
		<div class="header-sub-wrapper-2">
			<div class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="navbar-collapse collapse">
					<!-- <ul class="nav navbar-nav">
						<li class="system-title"><?php echo $this->Global->Settings('system_title') ?></li>
					</ul> -->

					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#/customer-logs">
								<div class="menu-icon"><span class="svg-icon menu-icon-span" data-icon="bill"></span></div>
								<div class="menu-label">FRONT DESK</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><span class="svg-icon menu-icon-span" data-icon="group"></span></div>
								<div class="menu-label">MEMBERS</div>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#/members"><i class="fa fa-arrow-circle-right"></i> MEMBERS MANAGEMENT</a></li>
								<li><a href="#/membership-types"><i class="fa fa-arrow-circle-right"></i> MEMBERSHIP TYPES</a></li>
								<li><a href="#/share-capitals"><i class="fa fa-arrow-circle-right"></i> SHARE CAPITAL MANAGEMENT</a></li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><span class="svg-icon menu-icon-span" data-icon="lendingBook"></span></div>
								<div class="menu-label">LOANS</div>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#/loans"><i class="fa fa-arrow-circle-right"></i> LOANS MANAGEMENT</a></li>
								<li><a href="#/loan-types"><i class="fa fa-arrow-circle-right"></i> LOAN TYPES</a></li>
								<li><a href="#/daily-payments"><i class="fa fa-arrow-circle-right"></i> DAILY PAYMENTS</a></li>
								<li><a href="#/payment-notifications"><i class="fa fa-arrow-circle-right"></i> PAYMENT NOTIFICATIONS</a></li>
								<li><a href="#/collection-reports"><i class="fa fa-arrow-circle-right"></i> COLLECTION REPORT</a></li>
								<!-- <li><a href="#/deduction-exports"><i class="fa fa-arrow-circle-right"></i> DEDUCTION EXPORT</a></li>
								<li><a href="#/deduction-imports"><i class="fa fa-arrow-circle-right"></i> DEDUCTION IMPORT</a></li> -->
								<li><a href="#/interest-incomes"><i class="fa fa-arrow-circle-right"></i> INTEREST INCOME</a></li>
							</ul>
						</li>
						<li>
							<a href="#/savings">
								<div class="menu-icon"><span class="svg-icon menu-icon-span" data-icon="savings"></span></div>
								<div class="menu-label">SAVINGS</div>
							</a>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><span class="svg-icon menu-icon-span" data-icon="bill"></span></div>
								<div class="menu-label">ACCOUNTING</div>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#/accounts"><i class="fa fa-arrow-circle-right"></i> CHART OF ACCOUNTS</a></li>
								<li><a href="#/beginning-balance"><i class="fa fa-arrow-circle-right"></i> BEGGINING BALANCE <i>(<?php echo $this->Global->Settings('active_year') ?>)</i></a></li>
								<li><a href="#/accounting-entry-add"><i class="fa fa-arrow-circle-right"></i> NEW ENTRY</a></li>
								<li><a href="#/accounting-entries"><i class="fa fa-arrow-circle-right"></i> ACCOUNTING ENTRIES</a></li>
								<li><a href="#/cash-books"><i class="fa fa-arrow-circle-right"></i> CASH BOOK</a></li>
								<li><a href="#/accounting-books"><i class="fa fa-arrow-circle-right"></i> LEDGERS</a></li>
								<li><a href="#/trial-balance"><i class="fa fa-arrow-circle-right"></i> TRIAL BALANCE</a></li>
								<li><a href="#/balance-sheet"><i class="fa fa-arrow-circle-right"></i> BALANCE SHEET</a></li>
								<li><a href="#/income-statements"><i class="fa fa-arrow-circle-right"></i> INCOME STATEMENT</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#/inventories">
								<div class="menu-icon"><span class="svg-icon menu-icon-span" data-icon="bill"></span></div>
								<div class="menu-label">INVENTORY</div>
							</a>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><span class="svg-icon menu-icon-span" data-icon="settings"></span></div>
								<div class="menu-label">SETTINGS</div>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#/settings"><i class="fa fa-arrow-circle-right"></i> COOPERATIVE INFORMATION</a></li>
								<li><a href="#/departments"><i class="fa fa-arrow-circle-right"></i> DEPARTMENTS</a></li>
								<li><a href="#/patronage-refunds"><i class="fa fa-arrow-circle-right"></i> PATRONAGE REFUND</a></li>
								<li><a href="#/dividends"><i class="fa fa-arrow-circle-right"></i> DIVIDENDS</a></li>
								<li><a href="#/request-logs"><i class="fa fa-arrow-circle-right"></i> REQUEST LOG</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
