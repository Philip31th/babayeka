<div class="modal fade" data-backdrop="static" id="addItemModal">
 <div class="modal-dialog modal-vertical-centered">
  <div class="modal-content">
   <div class="modal-header">
    <b>{{modalMode}} ITEM</b>
    <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   </div>
   <div class="modal-body">
    <!---->  
    <div class="col-md-12">
     <div class="form-group">
      <dt>CODE:<i class="required">*</i></dt>
      <input type="text" class="form-control input-sm" ng-model="datas.Item.code">     
     </div>    
    </div>
    <!---->
    <div class="col-md-12">
     <div class="form-group">
      <dt>NAME<i class="required">*</i></dt> 
      <input type="text" class="form-control input-sm" ng-model="datas.Item.name">
     </div>    
    </div>
    <!---->
    <div class="col-md-12">
     <div class="form-group">
      <dt>DESCRIPTION:<i class="required">*</i></dt>
      <input type="text" class="input-sm form-control" ng-model="datas.Item.description">
     </div>    
    </div>
    <!---->
    <div class="col-md-12">
     <div class="form-group">
      <dt>SECTION:<i class="required">*</i></dt> 
      <select class="form-control uppercase" ng-model="datas.Item.sectionId">
       <option value="1">SECTION A</option>
       <option value="2">SECTION B</option>
      </select>
     </div>     
    </div> 
    <!---->
    <div class="col-md-6">
     <div class="form-group">
      <dt>EXPIRATION DATE:<i class="required">*</i></dt>
     </div>
     <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
      <input id="expirationDate" type="text" class="form-control input-sm datepicker" ng-model="datas.Item.expiration">
     </div>
    </div>
    <!---->
   </div>  
   <!---->
   <div class="modal-footer">
    <button class="btn btn-{{ modalMode == 'EDIT' ? 'success' : 'primary'}} btn-sm btn-min" ng-click="saveItemModal();"><i class="fa {{ modalMode =='EDIT' ? 'fa-floppy-o': 'fa-plus'}}"></i></button>
   </div>
  </div>   
 </div>    
</div>

<!---->
<style>
#addItemModal {
 top:-30%;
}
</style>