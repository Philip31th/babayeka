<div class="row">
 <div id="panelTop" class="panel panel-danger no-border-radius">
   <div class="panel panel-heading no-border-radius">ALERT</div>
   <div class"col-md-12">
     <div class="col-md-2">
      <div class="box btn-danger">
        <div>{{ countDay }}</div>
        <div class="labelExpire">Item(s) expire within 5 days!</div>
      </div>
     </div>
   </div>
 </div>
</div>
<style>
#panelTop{
 margin-bottom:0px;
 height:200px;
}  
.box{
   padding: 15px;
   text-align: center;
   font-weight: 300;
   font-size: 45px;
   color: white;
}
.labelExpire{
 font-size:12px;
}
.box:hover{
 -webkit-transition:scale(1.05);
 transform:scale(1.05);
}
</style>