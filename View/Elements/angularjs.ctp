<!-- angularjs -->
<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/angular/angular.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/angular/angular-route.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/angular/angular-resource.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/angular-loading/loading-bar.js"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/angular/angular-selectize.js"></script>

<!-- .angularjs -->

<!-- angularjs app -->
<script type="text/javascript" src="<?php echo $this->base ?>/app/app.js?version=<?php echo time() ?>"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/app/directives.js"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/app/filters.js"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/app/services.js"></script>
<script type="text/javascript" src="<?php echo $this->base ?>/app/controllers.js"></script>

<!-- angularjs scripts -->
<?php
  $scripts = array(
    'dashboard',
    'dbs'
  );
?>

<?php foreach ($scripts as $script): ?>
  <?php $script = str_replace('__', '/', $script) ?>
  <!-- <?php echo $script ?> -->
  <script type="text/javascript" src="<?php echo $base ?>app/<?php echo $script ?>/service.js<?php echo '?version=' . time() ?>"></script>
  <script type="text/javascript" src="<?php echo $base ?>app/<?php echo $script ?>/route.js<?php echo '?version=' . time() ?>"></script>
  <script type="text/javascript" src="<?php echo $base ?>app/<?php echo $script ?>/controller.js<?php echo '?version=' . time() ?>"></script>
  <!-- .<?php echo $script ?> -->
<?php endforeach ?>

