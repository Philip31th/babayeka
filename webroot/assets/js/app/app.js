var app = angular.module('ehrms', ['ngRoute', 'ngResource', 'chieffancypants.loadingBar', 'selectize']);

app.config(function($routeProvider) {
  $routeProvider
  .otherwise({
    redirectTo: '/dashboard'
  });
});
