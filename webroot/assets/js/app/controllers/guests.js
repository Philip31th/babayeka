// service
app.factory("GuestList", function($resource) {
  return $resource( api + "guestslists/:id", {id: '@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/guestslists', {
    templateUrl: base + 'guestslists',
    controller: 'FolioGuestListController',
  })
  .when('/guestslists/view/:id', {
    templateUrl: base + 'guestslists/view',
    controller: 'FolioGuestListViewController',
  })
  .when('/guestslists/edit/:id', {
    templateUrl: base + 'guestslists/edit',
    controller: 'FolioGuestListEditController',
  });
});

// controller
app.controller('FolioGuestListController', function($scope, GuestList){

GuestList.query(function(data) {
    $scope.allguestlist = data.result;
  });
});
app.controller('FolioGuestListViewController', function($scope, $routeParams, GuestList){
  $scope.id = $routeParams.id;
  $scope.load = function(){
    GuestList.get({id:$scope.id}, function(e){
        $scope.data = e.result;
      });
  }
  $scope.load();
});
app.controller('FolioGuestListEditController', function($scope, $routeParams, GuestList){
  $scope.id = $routeParams.id;
  $scope.load = function(){
    GuestList.get({id:$scope.id}, function(e){
        $scope.data = e.result;
      });
  }
  $scope.load();
  
  $scope.save = function(){
    valid = $("#form").validationEngine('validate');
    
    if(valid){
    GuestList.update({id:$scope.id}, $scope.data  ,function(e){
      if(e.response){
        $.gritter.add({ title: 'Successful!', text: e.message });
        window.location = '#/guestslists';
      }
    });
    }
  }
  
});