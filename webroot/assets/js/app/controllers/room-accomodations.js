// service
app.factory("Accomodation", function($resource) {
	return $resource( api + "accomodations/:id", { id: '@id', search: '@search' }, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' },
		search: { method: 'GET' },
	});
});

// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/room-accomodations', {
    templateUrl: base + 'accomodations',
    controller: 'RoomAccomodationController',
  })
  .when('/room-accomodations/add', {
    templateUrl: base + '/accomodations/add',
    controller: 'RoomAccomodationAddController',
  })
  .when('/room-accomodations/edit/:id', {
    templateUrl: base + '/accomodations/edit',
    controller: 'RoomAccomodationEditController',
  });
});


// controller
app.controller('RoomAccomodationController', function($scope,Accomodation){
	Accomodation.query(function(data) {
		$scope.datas = data.result;
	});
	
	$scope.remove = function(data){
		console.log($scope.data)
		msg = confirm('Are you sure you want to delete?');
		if(msg){
			Accomodation.remove({ id:data.id }, function(e){
				if(e.response){
					$.gritter.add({ title: 'Successful!', text: e.message });
					$scope.datas.splice( $scope.datas.indexOf(data), 1 );
				}
			});
			
		} 
	}
	
});
app.controller('RoomAccomodationAddController', function($scope,Accomodation){
	$scope.save = function(){
		console.log($scope.data);
		valid = $("#form").validationEngine('validate');
		if(valid){
		Accomodation.save($scope.data, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#/room-accomodations';
			}
		});
		}
	}
});
app.controller('RoomAccomodationEditController', function($scope,$routeParams,Accomodation){
	$scope.id = $routeParams.id;
	Accomodation.get({id:$scope.id}, function(e){
		$scope.data = e.result;
	});
	
	$scope.save = function(){
		valid = $("#form").validationEngine('validate');
		if(valid){
		Accomodation.update({id:$scope.id}, $scope.data, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#room-accomodations';
			}
		});
	  }
	}
});
