// service
app.factory("Cashiering", function($resource) {
	return $resource( api + "cashiering/:id", { id: '@id', search: '@search' }, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' },
		search: { method: 'GET' },
	});
});
app.factory("ReservationCharges", function($resource) {
  return $resource( api + "reservation-charges/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});
app.factory("FolioPayment", function($resource) {
  return $resource( api + "folio-payments/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});
app.factory("ReportCashiering", function($resource) {
  return $resource( api + "cashiering/report", {}, {query:{method:'GET', isArray:false}});
});

// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/cashiering', {
    templateUrl: base + 'cashiering',
    controller: 'CashieringController',
  })
  .when('/cashiering/view/:id', {
    templateUrl: base + 'cashiering/view',
    controller: 'CashieringViewController',
  })
  .when('/reports/cashiering', {
    templateUrl: base + 'cashiering/report',
    controller: 'CashieringReportController',
  })
  .when('/reports/night-audit', {
    templateUrl: base + 'cashiering/audit',
    controller: 'CashieringAuditController',
  });
});


// controller
app.controller('CashieringController', function($scope, Cashiering){
  
	$scope.strSearch = Date.parse('today').toString('yyyy-MM-dd');
	$scope.datas = {};
	
	$scope.load = function(){
		Cashiering.query(function(data) {
			$scope.datas = data.result;
		});
	}
	$scope.load();
	
	$scope.search = function(strSearch){
		$scope.load();
	};
});
app.controller('CashieringAddController', function($scope, Cashiering){

});
app.controller('CashieringViewController', function($scope, $routeParams, Cashiering, ReservationCharges, FolioPayment, Charge, Transaction){
  $scope.id = $routeParams.id;
  modalMaxHeight();
  
  $('.datepicker').datepicker({format: 'mm/dd/yyyy', autoclose: true});
  $scope.load = function(){
    Cashiering.get({id:$scope.id}, function(e){
      $scope.data = e.result;
    });
  }
  
	$scope.load();
	Charge.query(function(e){
	  $scope.chargesSelection = e.result
	});

	// folio payment
	$scope.addPayment = function(data){
	  $scope.afoliopayment = {
	    FolioPayment:{
	      folio_id: $scope.id,
	      transaction_id: data.id,
	      date: Date.today().toString('MM/dd/yyyy')
	    }
	  }
	  $scope.paymentMaxValue = data.balance;
	  log($scope.paymentMaxValue);
    $('#add-payment-modal').modal('show');
	}
	$scope.savePayment = function(){
	  FolioPayment.save($scope.afoliopayment, function(e){
	    if(e.response){
	      $('#add-payment-modal').modal('hide');
	      $scope.load();
	      $scope.paymentMaxValue = 0;
	    }
	  });
	}
	// .folio payment
	

	// checkout
  $scope.checkOut = function(){
    bootbox.confirm('Are you sure you want to checkout this transaction', function(e){
      console.log(e);
      if(e){
        Cashiering.update({id:$scope.id},
          {
            folio_id: $scope.data.id,
            reservation_id: $scope.data.reservation_id,
            reservation_room_id: $scope.data.reservation_room_id
          },
          function(e){
            if(e.response){
              $.gritter.add({title:'Successful!', text:'Booking has been checked out.'});
              $scope.load();
            }
        });
      }
    });
  }
  // .checkout
  
  // add charges
  $scope.addCharges = function(){
	  $('#add-charges-modal').modal('show');
	  $scope.atransaction = {
	    Transaction:{
	      business_id: 1,
	      particulars: 'Hotel other charges.',
	      title: 'Other charges.'
	    },
	    TransactionSub: [],
	    FolioTransaction: {
	      folio_id: $scope.data.id
	    }
	  }
	}
  $scope.saveCharges = function(){
    angular.forEach($scope.chargesSelection, function(charge, e) {
      if(charge.selected){
        $scope.atransaction.TransactionSub.push({
          particulars: charge.name,
          amount: charge.amount
        });
      }
    });
    Transaction.save($scope.atransaction, function(e){
      if(e.response){
        angular.forEach($scope.chargesSelection, function(charge, e) {
          charge.selected = false;
        });
        $scope.load();
        $('#add-charges-modal').modal('hide');
      }
    });
  }
  // .add charges
  
  $scope.bool = [{value: true, name: 'FIX AMOUNT'}, {value: false, name: 'PERCENTAGE'}]
  // change discount
  $scope.changeDiscount = function(data){
    $('#change-discount-modal').modal('show');
    Transaction.get({id:data.id}, function(e){
      $scope.ediscount = {
        Transaction:{
          id:  e.result.Transaction.id,
          discount_type: e.result.Transaction.discount_type,
          discount: e.result.Transaction.discount
        }
      }
    });
  }
  $scope.saveDiscount = function(){
    Transaction.update({ id: $scope.ediscount.Transaction.id }, $scope.ediscount, function(e){
      if(e.response){
        $.gritter.add({title:'Successful!', text:'Transaction discount has been saved.'});
        $scope.load();
        $('#change-discount-modal').modal('hide');
      }
    });
  }
  // .change discount
});
app.controller('CashieringEditController', function($scope, $routeParams, Cashiering){

});
app.controller('CashieringReportController', function($scope, ReportCashiering){
  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('#search').datepicker({ format: 'mm/dd/yyyy', autoclose: true});

  $scope.strSearch = Date.parse('today').toString('yyyy-MM-dd');
  $scope.datas = {};
  $scope.load = function(){
    ReportCashiering.query(function(data) {
      $scope.datas = data.result;
    });
  }
  $scope.load();
  
  $scope.search = function(strSearch){
    $scope.load();
  };
});
app.controller('CashieringAuditController', function($scope, $routeParams, Cashiering){

});