// service
app.factory("Discount", function($resource) {
  return $resource( api + "discounts/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});




// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/discounts', {
    templateUrl: base + 'discounts',
    controller: 'DiscountController',
  })
  .when('/discounts/add', {
    templateUrl: base + '/discounts/add',
    controller: 'AddDiscountsController',
  })
  .when('/discounts/edit/:id', {
    templateUrl: base + '/discounts/edit',
    controller: 'EditDiscountsController',
  });
});


// controller
app.controller('DiscountController', function($scope,Discount,CheckLevel){
  
  CheckLevel.query({ id : userId }, function (e) {
    if (e.ok) $scope.highLevel = e.data; 
  });
  
	Discount.query(function(data) {
		$scope.datas = data.result;
	});
	$scope.remove = function(data){
		console.log($scope.data)
		msg = confirm('Are you sure you want to delete?');
		if(msg){
			Discount.remove({ id:data.id }, function(e){
				if(e.response){
					$.gritter.add({ title: 'Successful!', text: e.message });
					$scope.datas.splice( $scope.datas.indexOf(data), 1 );
				}
			});
			
		} 
	}
});

app.controller('AddDiscountsController', function($scope,Discount){
	$scope.save = function(){
		console.log($scope.data);
		valid = $("#form").validationEngine('validate');
		if(valid){
		Discount.save($scope.data, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#/discounts';
			}
		});
	  }
	}
});

app.controller('EditDiscountsController', function($scope,$routeParams,Discount){
		$scope.id = $routeParams.id;
	Discount.get({id:$scope.id}, function(e){
		$scope.data = e.result;
	});
	$scope.save = function(){
		valid = $("#form").validationEngine('validate');
		if(valid){
		Discount.update({id:$scope.id}, $scope.data, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#/discounts';
			}
		});
	  }
	}
});