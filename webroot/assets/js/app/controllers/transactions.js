// service
app.factory("Transaction", function($resource) {
	return $resource( api + "transactions/:id", { id: '@id'}, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' }
	});
});


// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/transactions', {
    templateUrl: base + 'transactions',
    controller: 'TransactionsController',
  })
  .when('/transactions/add', {
    templateUrl: base + 'transactions/add',
    controller: 'TransactionAddController',
  });
});


// controller
app.controller('TransactionController', function($scope, Transaction){

	$scope.load = function(){
		Transaction.query(function(data) {
			$scope.transactions = data.result;
		});
	}
	$scope.load();
	
	$scope.remove = function(transaction){
		console.log($scope.transaction)
		msg = confirm('Are you sure you want to delete?');
		if(msg){
			Transaction.remove({ id:transaction.id }, function(e){
				if(e.response){
					$.gritter.add({ title: 'Successful!', text: e.message });
					$scope.transactions.splice( $scope.transactions.indexOf(data), 1 );
				}
			});
			
		} 
	}
});
app.controller('TransactionAddController', function($scope, Transaction){
	$('#form').validationEngine('attach');
	$('.datepicker').datepicker({format:'mm/dd/yyyy', autoclose:true});
	$scope.save = function(){
		valid = $("#form").validationEngine('validate');
		if(valid){
			Transaction.save($scope.data, function(e){
				if(e.response){
					$.gritter.add({ title: 'Successful!', text: e.message });
					window.location = '#rooms';
				}
			});
		}
	}
	
	
	// add transaction item
  $scope.atransaction = {
    TransactionSub: []
  }
	$scope.addItem = function(){
	  $('#new-item-modal').modal('show');
	}
	$scope.saveItem = function(){
	  $scope.atransaction.TransactionSub.push($scope.atransactionsub);
	  $scope.atransactionsub = {}
	  $('#new-item-modal').modal('hide');
	}
	// .add transaction item
});
app.controller('TransactionViewController', function($scope, $stateParams, Room){

});
app.controller('TransactionEditController', function($scope, $stateParams, Transaction){
	Transaction.get({id:$stateParams.id}, function(e){
		$scope.data = e.result;
	});
	
	$scope.save = function(){
		Transaction.update({id:$stateParams.id}, $scope.data, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#transaction';
			}
		});
	}
});