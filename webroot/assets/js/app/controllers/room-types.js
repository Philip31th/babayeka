// service
app.factory("RoomType", function($resource) {
	return $resource( api + "room-types/:id", { id: '@id', search: '@search' }, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' },
		search: { method: 'GET' },
	});
});
app.factory("AvailableRoomType", function($resource) {
  return $resource( api + "room_types/available", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});


// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/room-types', {
    templateUrl: base + 'room-types',
    controller: 'RoomTypeController',
  })
  .when('/room-types/add', {
    templateUrl: base + 'room-types/add',
    controller: 'RoomTypeAddController',
  })
	.when('/room-types/edit/:id', {
    templateUrl: base + '/room-types/edit',
    controller: 'RoomTypeEditController',
  })
	.when('/room-types/view/:id', {
    templateUrl: base + '/room-types/view',
    controller: 'RoomTypeViewController',
  });
});


// controller
app.controller('RoomTypeController', function($scope, RoomType){
	
	$scope.load = function(){
    $scope.roomTypes = RoomType.query({ date:$scope.strSearch }, function(data) {
      $scope.roomTypes = data.result;
      $scope.paginator = data.paginator;
      $scope.endPage = $scope.paginator.pageCount < $scope.showPage? $scope.paginator.pageCount:$scope.showPage;
      $scope.showPage = $scope.paginator.pageCount < $scope.showPage? $scope.paginator.pageCount:$scope.showPage;
    });
	};
    // $scope.load = function(){
        // RoomType.query(function(e){
            // $scope.roomTypes = e.result;
        // });
    // }
    $scope.load();
		
    $scope.remove = function(roomtype){
        cnf = confirm('Are you sure you want to delete this room type?');
        if(cnf){
            RoomType.remove({id:roomtype.id}, function(e){
                if(e.response){
                    $scope.load();
                }
            });
        }
    };
		$scope.search = function(strSearch){
		RoomType.search({search:strSearch}, function(data) {
			$scope.roomTypes = data.result;
		});
	};
		$scope.page = function(page){
		if(page >= 1 && page <= $scope.paginator.pageCount){
			RoomType.search({page:page}, function(data) {
				$scope.datas = data.result;
			});
			$scope.paginator.page = page;
			if(page <= Math.round($scope.showPage/2)) $scope.startPage = 1;
			else if(page > $scope.paginator.pageCount-Math.round($scope.showPage/2)) $scope.startPage = $scope.paginator.pageCount-($scope.showPage-1);
			else $scope.startPage = page-Math.floor($scope.showPage/2);
			if($scope.startPage + ($scope.showPage-1) > $scope.paginator.pageCount){
				$scope.startPage = $scope.paginator.pageCount - ($scope.showPage-1);
				$scope.endPage = $scope.paginator.pageCount;
			} else $scope.endPage = $scope.startPage + ($scope.showPage-1);
			$('.pagination-page').removeClass('active');
			$('li[data-page='+ page +']').addClass('active');
		}
	};
});
app.controller('RoomTypeAddController', function($scope, RoomType){
    $('#form').validationEngine('attach');
    $scope.save = function(){
        valid = $("#form").validationEngine('validate');
        if(valid){
            RoomType.save($scope.aroomtype, function(e){
    			if(e.response){
    				$.gritter.add({ title: 'Successful!', text: e.message });
    				window.location = '#/room-types';
    			}
            });
        } 
    }
});

app.controller('RoomTypeEditController', function($scope,$routeParams,RoomType){
	$scope.id = $routeParams.id;
	RoomType.get({id:$scope.id}, function(e){
		$scope.aroomtype = e.result;
	});
	$scope.save = function(){
		valid = $("#form").validationEngine('validate');
		if(valid){
		RoomType.update({id:$scope.id}, $scope.aroomtype, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#/room-types';
			}
		});
	  }
	}
});
app.controller('RoomTypeViewController', function($scope, $routeParams, RoomType){
$scope.id = $routeParams.id;
RoomType.get({id:$scope.id}, function(e){
		$scope.aroomtype = e.result;
	});
});

