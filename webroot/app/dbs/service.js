app.factory("Dbs", function($resource) {
  return $resource( api + "dbs/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' }
  });
});
