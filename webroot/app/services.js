app.factory("Select", function($resource) {
  return $resource( api + "select", { }, { query: { method: 'GET', isArray: false }});
});
app.factory("Integration", function($resource) {
  return $resource( api + "integrations/:id", { id: '@id', search: '@search' }, {
     query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});
