app.controller('SettingController', function($scope, Setting) {

  // load settings
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Setting.query(options, function(e) {
      if (e.ok) {
        $scope.settings = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove settings
  $scope.remove = function(setting) {
    bootbox.confirm('Are you sure you want to delete ' + setting.name + '?', function(b) {
      if (b) {
        Setting.remove({ id: setting.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            $scope.load({
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
        });
      }
    });
  }
});


app.controller('SettingAddController', function($scope, Setting) {
  $('#form').validationEngine('attach');

  // save setting
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      Setting.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/settings';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});


app.controller('SettingEditController', function($scope, $routeParams, Setting) {
  $('#form').validationEngine('attach');
  $scope.settingId = $routeParams.id;

  // load setting
  $scope.load = function() {
    Setting.get({ id: $scope.settingId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update setting
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      Setting.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/settings';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});


app.controller('SettingViewController', function($scope, $routeParams, Setting) {
  $scope.settingId = $routeParams.id;

  // load setting
  $scope.load = function() {
    Setting.get({ id: $scope.settingId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove settings
  $scope.remove = function(setting) {
    bootbox.confirm('Are you sure you want to delete ' + setting.name + '?', function(b) {
      if (b) {
        Setting.remove({ id: setting.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });
            window.location = '#/settings';
          }
        });
      }
    });
  }
});