app.factory("Setting", function($resource) {
  return $resource( api + "settings/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});