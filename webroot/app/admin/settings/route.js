app.config(function($routeProvider) {
  $routeProvider
  .when('/settings', {
    templateUrl: tmp + 'admin__settings__index',
    controller: 'SettingController',
  })
  .when('/setting/view/:id', {
    templateUrl: tmp + 'admin__settings__view',
    controller: 'SettingViewController',
  })
  .when('/setting/add', {
    templateUrl: tmp + 'admin__settings__add',
    controller: 'SettingAddController',
  })
  .when('/setting/edit/:id', {
    templateUrl: tmp + 'admin__settings__edit',
    controller: 'SettingEditController',
  });
});