-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2017 at 06:45 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `integration`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `connectionTypeId` int(11) NOT NULL,
  `manufacturerId` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `softwareVersion` varchar(255) NOT NULL,
  `distributor` varchar(255) NOT NULL,
  `driverVersion` varchar(255) NOT NULL,
  `driverModId` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `imgSrc` varchar(255) DEFAULT NULL,
  `driverpath` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `code`, `sectionId`, `connectionTypeId`, `manufacturerId`, `model`, `softwareVersion`, `distributor`, `driverVersion`, `driverModId`, `status`, `imgSrc`, `driverpath`, `visible`, `created`, `modified`) VALUES
(51, 'H2B01-0010', 2, 1, 1, 'DXH800', '1.1.1.1.6', 'MDMPI', '1.0.0.2.2', 4, 1, 'glb_bci_003106.jpg', NULL, 0, '2016-06-15 09:12:47', '2016-06-21 10:02:01'),
(52, 'H1B01-0001', 1, 1, 1, 'DXH800', 'Os Dos 1.0', 'MDMPI', '1.0.0.2.2', 4, 1, 'glb_bci_003106.jpg', 'Host_ACLELITE.dll', 1, '2016-06-15 09:22:20', '2017-01-18 08:12:49'),
(53, 'H1B01-0002', 1, 1, 1, 'LH750', 'MyOs', 'MDMPI', '1.0.0.2', 1, 1, 'LH750.jpg', '53', 1, '2016-06-21 04:03:56', '2017-01-13 16:08:25'),
(54, 'H1B01-0003', 1, 1, 1, 'HMX', 'OtherOs', 'MDMPI', '1.0.0.27', 1, 1, '54.jpg', NULL, 1, '2016-06-21 05:36:39', '2016-06-22 08:38:06'),
(55, 'H1B01-0004', 1, 1, 1, 'ACT5DIFF', 'OtherOs', 'MDMPI', '1.0.0.9', 1, 1, '55.jpg', NULL, 1, '2016-06-21 05:39:51', '2016-06-22 08:45:24'),
(56, 'H1B01-0005', 1, 1, 1, 'ACTDIFF', 'OtherOs', 'MDMPI', '1.0.0.19', 4, 1, '56.jpg', NULL, 0, '2016-06-21 05:53:59', '2016-07-14 07:25:43'),
(62, 'H2B01-0002', 2, 1, 1, 'ewqe', 'eqweqw', 'eqweqwe', 'ewqeqwe', 1, 1, 'ACT5DIFF.jpg', NULL, 0, '2016-06-21 09:05:58', '2016-06-22 08:46:02'),
(63, 'H1B02-0001', 1, 1, 2, 'eqweqw', 'eqwewq', 'ewqeqw', 'eqweqw', 1, 1, 'ACT5DIFF.jpg', NULL, 0, '2016-06-21 09:28:16', '2016-06-22 08:50:08'),
(64, 'H1B01-0006', 1, 1, 1, 'ewew', 'qewqe', 'wqewq', 'ewqewqewqe', 4, 1, NULL, NULL, 0, '2016-06-22 02:23:49', '2016-06-22 08:50:06'),
(65, 'H1B01-0007', 1, 1, 1, 'ewqe', 'qweqw', 'eqwewq', 'eqweq', 2, 1, '65.jpg', NULL, 0, '2016-06-22 02:25:34', '2016-06-22 08:50:11'),
(66, 'H1B01-0008', 1, 1, 1, 'ewqewq', 'ewqe', 'wqe', 'wqewqew', 2, 1, '66.jpg', NULL, 0, '2016-06-22 02:30:52', '2016-06-22 08:50:13'),
(67, 'H1B02-0002', 1, 1, 2, 'eqwe', 'qweqwe', 'wqeqw', 'eqweqw', 4, 1, '67.jpg', NULL, 0, '2016-06-22 02:31:34', '2016-06-22 08:50:04'),
(68, 'H1B02-0003', 1, 1, 2, 'DXH500', 'eqweqweqwe', 'ewq', 'ewqewq', 1, 1, '68.jpg', NULL, 0, '2016-06-27 08:54:17', '2016-07-14 07:25:41'),
(69, 'H1B02-0004', 1, 1, 2, 'MYMODEL 300', 'ISv3', 'MDMPI', '9.0.0.1', 4, 1, '69.jpg', NULL, 0, '2016-07-07 10:29:22', '2016-07-14 07:25:38'),
(70, 'H1B01-0009', 1, 1, 1, 'h1h1h1h1', 'ISV3', 'MDMPI', '1.0.0', 2, 1, '70.jpg', NULL, 0, '2016-07-07 10:32:52', '2016-07-14 07:25:36'),
(71, 'H1B01-0010', 1, 1, 1, 'eqwe', 'qwew', 'qeqwe', 'qwewq', 1, 1, '71.jpg', NULL, 0, '2016-07-07 10:36:32', '2016-07-14 07:25:34'),
(72, 'H1B02-0005', 1, 1, 2, 'GEM3000', 'NA', 'MDMPI', 'NA', 4, 1, '72.jpg', 'Host_GEM3500.dll', 1, '2016-07-14 07:34:30', '2017-02-08 14:24:18'),
(73, 'C1B01-0001', 6, 1, 1, 'UniCel DxC 800', 'Undetermined', 'MDMPI', '1.0', 4, 1, '73.jpg', NULL, 1, '2016-07-15 16:51:53', '2016-07-15 16:51:53'),
(74, 'C1B02-0001', 6, 1, 2, 'ILAB 300', '1', 'MDMPI', '1', 4, 1, '74.jpg', NULL, 1, '2016-09-14 10:46:18', '2016-09-14 10:46:18'),
(75, 'C1UHABX-0001', 6, 10, 6, 'PENTRA 400 ABX', 'RAA025FA', 'ZION DIAGNOSTICS SOLUTIONS', '1.0.0.1', 2, 1, '75.jpg', NULL, 1, '2016-12-19 14:33:59', '2016-12-19 14:33:59'),
(76, 'H2B01-0003', 2, 1, 1, 'eqwe', 'qweqw', 'ewqe', 'eqwe', 1, 1, '76.jpg', 'Host_Access2.dll', 0, '2017-01-17 16:54:04', '2017-01-17 16:55:45');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `code`, `name`, `description`, `visible`, `created`, `modified`) VALUES
(1, 'green', NULL, 'ADD MACHINE', 1, '2016-07-01 00:00:00', NULL),
(2, 'red', NULL, 'DELETE MACHINE', 1, '2016-07-15 00:00:00', NULL),
(3, 'yellow', NULL, 'EDIT SOLUTION', 1, '2016-07-04 00:00:00', NULL),
(4, 'green', NULL, 'ADD SOLUTION', 1, '2016-07-14 00:00:00', NULL),
(5, 'red', NULL, 'DELETE SOLUTION', 1, '2016-07-06 00:00:00', NULL),
(6, 'green', NULL, 'ADD SOLUTION', 1, '2016-07-10 00:00:00', NULL),
(7, 'blue', NULL, 'VIEW SOLUTION', 1, '2016-07-29 00:00:00', NULL),
(8, 'blue', NULL, 'VIEW SOLUTION', 1, '2016-07-29 00:00:00', NULL),
(9, 'green', NULL, 'ADD SOLUTION', 1, '2016-07-02 00:00:00', NULL),
(10, 'yellow', NULL, 'EDIT SOLUTION', 1, '2016-07-17 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `log_subs`
--

CREATE TABLE `log_subs` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `code`, `name`, `description`, `visible`, `created`, `modified`) VALUES
(1, 'superuser', 'Superuser', NULL, 1, NULL, NULL),
(2, 'admin', 'Administrator', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `code`, `name`, `value`, `visible`, `created`, `modified`) VALUES
(1, 'integration', 'Integration', '', 1, NULL, '2014-11-14 04:58:14'),
(2, 'address', 'Address', '-', 1, NULL, '2014-10-23 04:21:38'),
(3, 'logo', 'Logo', NULL, 0, NULL, NULL),
(4, 'email', 'Email', NULL, 0, NULL, NULL),
(5, 'telephone', 'Telephone', '+63 00 000 0000', 1, NULL, '2014-10-23 04:21:53'),
(6, 'fax', 'Fax', NULL, 0, NULL, NULL),
(7, 'chairman', 'Chairman', NULL, 1, NULL, NULL),
(8, 'general_manager', 'General Manager', NULL, 1, NULL, NULL),
(11, 'mdmpi', 'Babayeka', 'B', 0, NULL, NULL),
(12, 'system_title', 'System Title', 'Diagnostic Screen', 1, NULL, '2014-11-25 01:58:32'),
(13, 'active_year', 'Active Year', '2017', 1, '2014-11-25 00:00:00', '2014-11-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `code`, `description`, `visible`, `created`, `modified`, `name`) VALUES
(1, 'CBC', 'CBC', 0, '0000-00-00 00:00:00', '2017-02-08 14:50:01', NULL),
(2, 'P2', 'p2', 1, NULL, '2017-02-17 09:00:03', NULL),
(3, 'pi02', 'pi02', 1, NULL, NULL, NULL),
(4, 'PTT', 'PTT', 1, '2017-01-06 09:25:50', '2017-01-06 09:25:50', NULL),
(5, 'TTP', 'TTP', 1, '2017-01-06 09:26:25', '2017-01-06 09:26:25', NULL),
(6, 'CA', 'Calcium', 1, '2017-01-13 11:21:45', '2017-01-13 11:21:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `employeeId` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `developer` tinyint(1) DEFAULT '0',
  `highLevel` tinyint(1) NOT NULL DEFAULT '0',
  `activated` tinyint(1) DEFAULT '0',
  `image` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `lastName`, `firstName`, `middleName`, `employeeId`, `role`, `developer`, `highLevel`, `activated`, `image`, `visible`, `created`, `modified`) VALUES
(1, 'red', 'c632fd08461ed3619f9b9234e128ace87bcc6d7b', 'Anciro', 'Wilson', NULL, NULL, 'superuser', 1, 0, 1, NULL, 1, '2014-10-13 00:00:00', '2016-07-08 16:55:07'),
(6, 'admin', '5d00e235af81508fddf417255dcf511586738d41', '-', '- Administrator', NULL, NULL, 'admin', 0, 0, 1, NULL, 1, '2014-10-13 00:00:00', '2015-06-26 14:18:14'),
(12, 'user', 'c632fd08461ed3619f9b9234e128ace87bcc6d7b', 'Ordiz', 'User', NULL, NULL, 'admin', 1, 0, 1, NULL, 1, '2015-06-03 18:19:29', '2017-02-27 13:39:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_subs`
--
ALTER TABLE `log_subs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `log_subs`
--
ALTER TABLE `log_subs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
