<?php
 class IntegrationsController extends AppController{
  public $layout = null;
  public $uses   = array(

  );
  
  public function beforeFilter(){
   parent::beforeFilter();
   $this->RequestHandler->ext = 'json';
   		$this->Auth->allow(array(
  			'index','view'
		));
  }
  public function index(){
   $rNum = null;
   $cNum  = null;
   if (isset($this->request->query['rNum']))
   $rNum = $this->request->query['rNum'];
   if (isset($this->request->query['cNum']))
   $cNum = $this->request->query['cNum'];
   
   $url = "http://172.16.187.201:8080/openmrs/ws/rest/v1/patient?q=$cNum";
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
   curl_setopt($ch, CURLOPT_USERPWD,"abg_contractor:Abg_contractor1");
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
   curl_setopt($ch, CURLOPT_URL,$url);
   $resultPatientIni = curl_exec($ch);
   curl_close($ch);
   $rPI = json_decode($resultPatientIni,true);
   $conceptId = $rPI['results'][0]['uuid'];
   if(!empty($rPI)){
    $url = "http://172.16.187.201:8080/openmrs/ws/rest/v1/person/$conceptId";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD,"abg_contractor:Abg_contractor1");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_URL,$url);
    $demographics = curl_exec($ch);
    $dgs = json_decode($demographics,true);
    curl_close($ch);
    $nameId = $dgs['preferredName']['uuid'];
     if(!empty($dgs)){
       $url = "http://172.16.187.201:8080/openmrs/ws/rest/v1/person/$conceptId/name/$nameId";
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
       curl_setopt($ch, CURLOPT_USERPWD,"abg_contractor:Abg_contractor1");
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
       curl_setopt($ch, CURLOPT_URL,$url);
       $fullName = curl_exec($ch);
       $fullNameInfo = json_decode($fullName,true);
       curl_close($ch);
      }
    $patientDetails = array(
     'NewPatientName' => $fullNameInfo['familyName'] .','. $fullNameInfo['givenName'] .' '. $fullNameInfo['middleName'] ,
     'Gender'         => $dgs['gender'],
     'Age'            => $dgs['age'],
     'BirthDate'      => $dgs['birthdate']
    );
     $admin = 'admin';
     $password = 'instantiation';
     $dbName = "C:\Users\MDMPI\Documents\Laboratory Instrument Interface 3\Data\InstrumentResults.mdb";
     if (!file_exists($dbName)) {
         die("Could not find database file.");
     }
     $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=$admin; Pwd=$password;");
     $sql  = "UPDATE tbLabInstrumentResults";
     $sql .= " SET  NewPatientName=" ."'".$patientDetails['NewPatientName']."'".","."Sex="."'".$patientDetails['Gender']."'".","."Age="."'".$patientDetails['Age']."'".","."BirthDate="."'".fdate($patientDetails['BirthDate'])."'".","."PatientName="."'".$patientDetails['NewPatientName']."'";
     $sql .=" WHERE ResultID=". "'" . $rNum . "'";
     $db->query($sql);     
   }

  $response = array(
    'ok'       => true,
    'data'     => $patientDetails,
  //  'tmpData'  => $dgs,
   // 'sql'      => $sql,
  //  'db'  => $db,
  //  'fullname' => $fullNameInfo
   );

   $this->set(array(
    'response'   => $response,
    '_serialize' => 'response'
   ));
  }
/*  public function view($id){

   if(isset($this->request->query['rNum']))
   $rNum = $this->request->query['rNum'];
   $online = null;
   $starttime = microtime(true);
   $file      = @fsockopen ('172.16.187.201', 8080, $errno, $errstr, 5);
   $stoptime  = microtime(true);
   $status    = 0;  
   
   if (!$file) {
      $online = 0;
      $status = -1;  // Site is down
   } else {
     fclose($file);
     $status = ($stoptime - $starttime) * 1000;
     $status = floor($status);
     $online = 1;    
   }
  
    $admin = 'sa';
    $password = 'instantiation';
    $dbName = "C:\Users\MDMPI\Documents\Laboratory Instrument Interface 3\Data\InstrumentResults.mdb";
    if (!file_exists($dbName)) {
        die("Could not find database file.");
    }
    $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=$admin; Pwd=$password;");
    
    $sql  = "UPDATE tbLabInstrumentResults SET NewPatientName=, Sex, Age,BirthDate  WHERE IDnum =" ."'". $id . "'". "AND ResultID=". "'" . $rNum . "'";

    $result = $db->query($sql);
    $row = $result->fetch();
   
    
   $response = array(
     '1'     => $online,
     '2'     => empty($row)? 0 : 1,
     'sql'   => $sql,
    );
 
    $this->set(array(
     'response'   => $response,
     '_serialize' => 'response'
    ));   
  }*/

}