<?php
 class ItemsController extends AppController{
  public $layout = null;
  public $uses   = array(
     'Item'
  );
  
  public function beforeFilter(){
   parent::beforeFilter();
   $this->RequestHandler->ext = 'json';
   		$this->Auth->allow(array(
  			'index'
		));
  }
  public function index(){
   

   $response = array(
    'ok'       => true,
    'data'     => '',
   );
   $this->set(array(
    'response'   => $response,
    '_serialize' => 'response'
   ));
  }

}