<?php
class SelectController extends AppController {

  public $layout = null;
  
  public function beforeFilter () {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
		$this->Auth->allow(array(
  			'index'
		));    
  }

}
