<?php
 class SectionsController extends AppController{
  public $layout = null;
  public $usus   = array(
     'Section'
  );
  
  public function beforeFilter(){
   parent::beforeFilter();
   $this->RequestHandler->ext = 'json';
  }
  public function index(){
   // page
   $page = isset($this->request->query['page']) ? $this->request->query : 1;
   //
   $conditions = array();
   $conditions['Section.visible'] = true ;
   //
   $paginatorSettings = array(
     'conditions' => $conditions,
     'limit'      => 25,
     'page'       => $page
   );
   $modelName = 'Section';
   $this->Paginator->settings = $paginatorSettings;
   $tmpData = $this->Paginator->paginate($modelName);
   $paginator = $this->request->params['paging'][$modelName];
   // transform data
   $sections = array();
   foreach($tmpData as $data){
    $section = $data['Section'];
    $sections[] = array(
     'id'          => $section['id'],
     'name'        => $section['name'],
     'code'        => $section['code'],
     'description' => $section['description']
    );
   }
   $response = array(
    'ok'   => true,
    'data' => $sections
   );
   $this->set(array(
    'response'   => $response,
    '_serialize' => 'response'
   ));
  }
}