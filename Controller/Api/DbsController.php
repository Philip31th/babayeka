<?php 
class DbsController extends AppController {
	  
	public $uses = array(
	 'Item',

  );
	public $layout = null;
	public $components = array('Paginator', 'RequestHandler');
	
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
	  
  $starttime = microtime(true);
  $file      = @fsockopen ('172.16.187.201', 8080, $errno, $errstr, 1);
  $stoptime  = microtime(true);
  $status    = 0;
  $msg;

  if (!$file) {
   $response = array(
      'ok'  => false,
      'msg' => 'Server is Offline'
    );
  $status = -1;  // Site is down
  } else {
    fclose($file);
    $status = ($stoptime - $starttime) * 1000;
    $status = floor($status);
    // access 
    $admin = 'sa';
    $password = 'instantiation';
    $dbName = "C:\Users\MDMPI\Documents\Laboratory Instrument Interface 3\Data\InstrumentResults.mdb";
    if (!file_exists($dbName)) {
        die("Could not find database file.");
    }
    $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=$admin; Pwd=$password;");
    
    $sql  = "SELECT TOP 30 PatientName,ResultDate FROM tbLabInstrumentResults";
    /*        if( ! $sth = $db->query("SELECT * FROM tbLabInstrumentResults") ) {
      die(var_export($db->errorinfo(), TRUE));
    }*/
    $result = $db->query($sql);
    $row = $result->fetchAll();
    $demographics = array();
    foreach ($row as $value){
      $demographics [] = array(
         'name' => $value['PatientName'],
         'date' => $value['ResultDate']
      );
    }
    $response = array(
      'ok'     => true,
      'msg'    => 'Server is Online',
      'status' => $result,
      'row'    => $demographics
    );      
  }

  $this->set(array(
    'response'=>$response,
    '_serialize'=>'response'
  ));
	}
}
