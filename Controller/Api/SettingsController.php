<?php 
class SettingsController extends AppController {
	
	public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    $conditions['Setting.visible'] = true;
    
    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];

      $conditions['OR'] = array(
        'Setting.name LIKE'  => "%$search%",
        'Setting.value LIKE' => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Setting.name' => 'ASC'
      )
    );
    $modelName = 'Setting';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];


    // transform data
    $settings = array();
    foreach ($tmpData as $data) {
      $setting = $data['Setting'];

      $settings[] = array(
        'id'    => $setting['id'],
        'code'  => $setting['code'],
        'name'  => $setting['name'],
        'value' => $setting['value'],
      );
    }

    $response = array(
      'ok'        => true,
      'data'      => $settings,
      'paginator' => $paginator
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $setting = $this->Setting->findById($id);

    $response = array(
      'ok'        => true,
      'data'      => $setting,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
   
  public function add() {
    $save = $this->Setting->validSave($this->request->data['Setting']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    $save = $this->Setting->validSave($this->request->data['Setting']);
    $response = $save;
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
	 
  public function delete($id = null) {
    if ($this->Setting->hide($id)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Setting has been deleted.',
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Setting cannot be delete this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
	
}
