<?php 
class TemplateController extends AppController {
	
  public $layout = null;
  public $autoRender = false;
  
  public function beforeFilter () {
    parent::beforeFilter();
  }

  public function afterFilter() {
    $view = str_replace('__', '/', $this->request->params['action']);
    $view = str_replace('_', '-', $view);
    $this->render($view);
  }

  public function dashboard__index() {}
  // 
  public function dbs__index() {}
  

}