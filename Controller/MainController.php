<?php
class MainController extends AppController {
	
	public $uses = array(
    'User',
    'Setting',
  );

  public $layout = null;
	public $components = array('RequestHandler');
    

  public function index() {
    $base = $this->serverUrl();
    $api  = $this->serverUrl() . 'api/';
    $tmp  = $this->serverUrl() . 'template/';

    $this->set(compact(
      'base',
      'api',
      'tmp'
    ));
  }

	public function login() {
    $this->User->save(array(
      'id'       => 12,
      'password' => 'password',
    ));
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect(array(
          'controller' => 'main',
          'action'     =>'index',
        ));
			}
		} else {
			if ($this->Auth->loggedIn()) {
        return $this->redirect(array(
          'controller' => 'main',
          'action'     => 'index',
        ));

			}
		}
    $this->set(compact('inventory', 'systemTitle'));
	}

	public function logout() {

		$this->Session->destroy();
		$this->Session->delete('Auth');
		return $this->redirect($this->Auth->logout());	
	}
}