<?php
Cache::config('default', array('engine' => 'File'));
App::build(array(
  'Controller' => array(
    ROOT . DS . APP_DIR . DS . 'Controller' . DS . 'Api' . DS
  )
));

Configure::write('Dispatcher.filters', array(
  'AssetDispatcher',
  'CacheDispatcher'
));

App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
  'engine' => 'File',
  'types' => array('notice', 'info', 'debug'),
  'file' => 'debug',
));

CakeLog::config('error', array(
  'engine' => 'File',
  'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
  'file' => 'error',
));

 
function d ($code) {
  var_dump($code);
}

function validate($data, $validate = 'notEmpty') {
  $notValid = false;
  if ($validate == 'notEmpty') {
    if (empty($data)) {
      $notValid = true;
    } elseif (!empty($data)) {
      $notValid = false;
    }

  } elseif ($validate == 'numeric') {
    if (!is_numeric($data)) {
      $notValid = true;
    } elseif (is_numeric($data)) {
      $notValid = false;
    }
  }

  return $notValid;
}

function slug ($string) {
  return strtolower(str_replace(' ', '-', $string));
}

function token() {
  return strval(bin2hex(openssl_random_pseudo_bytes(16)));
}

function fdate($date, $format = 'm/d/Y') {
  if (!empty($date))
    return date($format, strtotime($date));
  else
    return null;
}

function properCase($name = null) {
  return !empty($name)? ucwords(strtolower($name)) : '';
}

function rrmdir($dir) { 
 if (is_dir($dir)) {
   $objects = scandir($dir); 
   foreach ($objects as $object) { 
     if ($object != "." && $object != "..") { 
       if (filetype($dir."/".$object) == "dir")
        rrmdir($dir."/".$object);
       else
        unlink($dir . "/" . $object); 
     } 
   } 
   reset($objects); 
   rmdir($dir); 
 } 
}

function hasAccess($code = null, $user) {
  $result = false;
  if ($user['User']['developer'] OR $user['User']['highLevel']) {
    $result = true;
  } elseif (in_array($code, $user['UserPermission'])) {
    $result = true;
  }
  return $result;
}

