<?php
 class Item extends AppModel {
  public $recursive = -1;
  public $actsAs = array('Containable');
  
 	public $belongsTo = array(
  		'Section' => array(
  			'foreignKey' => 'sectionId'
  		)
  );
  public function visible($id = null,$value = true){
   $result = false;
   $this->id = $id;
   if($this->save(array('visible'=> $value))){
    $result = true; 
   } else {
    $result = false;
   }
   return $result;
  } 
 }
 
