<?php
class Setting extends AppModel {

  public function get($code = null) {
    $result = null;
    $data = $this->find('first', array(
      'conditions' => array(
        'code' => $code
      )
    ));

    $result = !empty($data)? $data['Setting']['value'] : null;
    return $result;
  }

  public function validSave($data) {
    $result = array();

    // transform data
    $data['code']  = slug(@$data['name']);
    $data['name']  = properCase(@$data['name']);
    $data['value'] = isset($data['value'])? $data['value'] : null;

    // validate name
    if (validate($data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Setting name is required.'
      );

    // validate setting value
    } elseif (validate($data['value'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Setting value is required.'
      );

    } else {
      $existingConditions = array();
      $existingConditions['name LIKE'] = $data['name'];
      $existingConditions['visible']   = true;

      if (isset($data['id']))
        $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Setting already exists.'
        );
      } else {
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Setting has been saved.'
          );
        }
      }
    }


    return $result;
  }

}